<?php

/* Cartelle di progetto*/
define("APP", __DIR__ . '/../app');
define("THIS_SERVER", 'https://do01.pegasodevs.win/server-comunica');
define("DOCUMENTS", __DIR__ . '/../documents');
define("ROUTES", __DIR__ . '/../app/routes');
define("PUBLIC", __DIR__);
define("PICTURES", __DIR__ . "/../public_html/images");
define("IMAGES_DIR", 'images');
define("IMAGES_PATH", '/var/www/html/server-comunica/public_html/images');
define("DS", DIRECTORY_SEPARATOR);

/* Inclusione autoloaders */
require __DIR__ . '/../app/vendor/autoload.php';
require __DIR__ . '/../app/autoloaders/LibsAutoloader.php';
require __DIR__ . '/../app/autoloaders/UtilsAutoloader.php';
require __DIR__ . '/../app/autoloaders/ModelAutoloader.php';

/* Registrazione autoloaders */
LibsAutoloader::registerAutoloader();
UtilsAutoloader::registerAutoloader();
ModelAutoloader::registerAutoloader();

/* Uses */
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;

/* Oggetto DB Globale */
/** @var DBLink[] $dbs */
//FIXME Connettersi soltanto ai db che servono
$dbs = DBLink::connectAll();

/* Oggetto FluentPDO globale */
/** @var FluentPDO[] $fps */
$fps = [];

/** @var DBLink $link */
foreach ($dbs as $connection => $link) {
    $fps[$connection] = new FluentPDO($link);
}

/* Container Slim */
/** @var \Slim\Container $c */
$c = new \Slim\Container();

/* Impostazione error handling */
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        /** @var \Slim\Container $c */
        return $c['response']->withJson(new ErrorResponse($exception));
    };
};

/* Impostazione not found handling */
$c['notFoundHandler'] = function ($c) {
    return function (SlimRequest $request, $response) use ($c) {
        /** @var \Slim\Container $c */
        if ($request->getMethod() == "OPTIONS") {
            return $c['response']->withJson(new SuccessResponse());
        }
        return $c['response']->withJson(new ErrorResponse(new Exception("Not found"), Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    };
};

/* Impostazione not allowed handling */
$c['notAllowedHandler'] = function ($c) {
    return function (SlimRequest $request, $response, $methods) use ($c) {
        /** @var \Slim\Container $c */
        if ($request->getMethod() == "OPTIONS") {
            return $c['response']->withJson(new SuccessResponse());
        }
        return $c['response']->withJson(new ErrorResponse(new Exception("Must be " . implode(', ', $methods)), Response::HTTP_NOT_ALLOWED), Response::HTTP_NOT_ALLOWED);
    };
};

/* Creazione directory per l'upload documenti */
/*if (file_exists(APP . "/platforms.config.json")) {
    $platforms = json_decode(file_get_contents(APP . "/platforms.config.json"), true);

    foreach ($platforms as $platform) {
        if (!file_exists(DOCUMENTS . DIRECTORY_SEPARATOR . $platform['name'])) {
            mkdir(DOCUMENTS . DIRECTORY_SEPARATOR . $platform['name']);
        }
    }
}*/


/* Oggetto App Globale */
$app = new \Slim\App($c);

/* Require delle routes */
foreach (glob(ROUTES . "/*.php") as $file) {
    require $file;
}


/* Route di default che mostra una pagina statica */
$app->get("/", function (SlimRequest $request, SlimResponse $response) {
    $html = <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>RESTful API</title>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<style type="text/css">
		body{
			margin: 0;
			font-family: 'Raleway', sans-serif
		}
		#wrapper{
			height: 100%;
			width: 100%;
			position: absolute;
		}
		#box{
			border-radius: 5px;
			border: 2px solid rgb(0, 0, 0);
			background-color: rgba(0, 0, 0, 0.3);
			padding: 15px;
			max-width:400px;
			min-width: 320px;
			width:80%;
			max-height:250px;
			margin: -125px auto 0 auto;
			text-align: center;
			position:absolute;
			top: 50%;
			left: 0;
			right: 0
		}
		.text{
			color: rgba(0,0,0, 1);
			text-shadow: 2px 2px 5px rgba(44,55,53, 1);
		}
		.text p{
			font-size: 2rem; margin: 10px 0 0 0
		}
	</style>
</head>
<body>
<div id="wrapper">
	<div id="box">
		<div class="text">
			<p>RESTful API</p>
		</div>
	</div>
</div>
</body>
</html>
HTML;

    return $response->getBody()->write($html);

});

$app->run();
