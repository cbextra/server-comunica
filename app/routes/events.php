<?php

use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;

$app->post('/event/upload-picture', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        //$productDetails = $request->getParsedBody();

        $event=new Event(0);

        return $response->withJson(new SuccessResponse($event->uploadPicture()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (BadFunctionCallException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/event', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $eventDetails = $request->getParsedBody();

        $event = new Event(0);

        return $response->withJson(new SuccessResponse($event->addEvent($eventDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/event/{eventId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);


        $event = new Event($args['eventId']);

        return $response->withJson(new SuccessResponse($event->getEventDetails()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/events', function (SlimRequest $request, SlimResponse $response, $args) {

    try {

        $sessionPayload = SessionManager::checkSession($request);
        if($sessionPayload){
            $userId=$sessionPayload->sub;
        }else {
            $userId=NULL;
        }

        $params=$request->getQueryParams();
        if(isset($params['term'])){
            $name=$params['term'];
        }else{
            $name=NULL;
        }

        $event = new Event(0);

        return $response->withJson(new SuccessResponse($event->getAllEvents($name, $userId)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->post('/event/{eventId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $eventDetails = $request->getParsedBody();


        $event = new Event($args['eventId']);

        return $response->withJson(new SuccessResponse($event->updateEvent($eventDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/event/{eventId}/visibility/{status}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $eventId = $args['eventId'];
        $status= $args['status'];
        $event = new Event($eventId);

        return $response->withJson(new SuccessResponse($event->setEventVisibility($status)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->delete('/event/{eventId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $event = new Event($args['eventId']);

        return $response->withJson(new SuccessResponse($event->deleteEvent()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


