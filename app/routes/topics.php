<?php


use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;


$app->post('/topic', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $topicDetails = $request->getParsedBody();

        $topic = new Topic(0);

        return $response->withJson(new SuccessResponse($topic->addTopic($topicDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/topic/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $topicId = $args['topicId'];

        $topic = new Topic($topicId);

        return $response->withJson(new SuccessResponse($topic->getTopicDetails()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/topics', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $params=$request->getQueryParams();
        if(isset($params['term'])){
            $name=$params['term'];
        }else{
            $name=NULL;
        }

        $topic = new Topic(0);

        return $response->withJson(new SuccessResponse($topic->getAllTopics($name)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->get('/topics/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $userId = $args['userId'];

        $topic = new Topic(0);

        return $response->withJson(new SuccessResponse($topic->getAllUserTopics($userId)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->post('/topic/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $topicDetails = $request->getParsedBody();

        $topicId = $args['topicId'];

        $topic = new Topic($topicId);

        return $response->withJson(new SuccessResponse($topic->updateTopic($topicDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->delete('/topic/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $topicId = $args['topicId'];

        $topic = new Topic($topicId);

        return $response->withJson(new SuccessResponse($topic->deleteTopic()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


