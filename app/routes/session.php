<?php
use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;

/**
 * @api {get} /session/check
 * @apiAuthenticationRequired no
 */

$app->get('/session/check', function (SlimRequest $request, SlimResponse $response) {
    $data = array();
    try {

        $sessionPayload = SessionManager::checkSession($request);
        $data['session'] = "valid";
        $data['expiresIn'] = $sessionPayload->exp - time();

        return $response->withJson(new SuccessResponse($data));

    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    }
});

$app->put('/session/logout', function (SlimRequest $request, SlimResponse $response) {
    $data = array();
    try {

        $sessionPayload = SessionManager::checkSession($request);
        $user= new User($sessionPayload->sub);
        return $response->withJson(new SuccessResponse($user->deletePushId($user->getAccountId())));

    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    }
});

$app->get('/session/token/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {
    return $response->withJson(new SuccessResponse(SessionManager::issueToken($args['userId'])));
});

$app->get('/if/is/puork', function (SlimRequest $request, SlimResponse $response, $args) {
    return $response->withJson(new SuccessResponse('Arrve\''));
});


$app->post('/session/login', function (SlimRequest $request, SlimResponse $response, $args) {
    global $dbs, $fps;
    $json = $request->getBody();
    $data = json_decode($json, true);

    try {
        $username = $data['username'];
        $password = $data['password'];

        if (empty($username)) {
            throw new BadFunctionCallException("Username cannot be empty");
        } elseif (empty($password)) {
            throw new BadFunctionCallException("Password cannot be empty");
        }

        $stmt = $dbs['comunica']->prepare('SELECT u.id, a.id as accountId, a.username, a.password FROM user_accounts a INNER JOIN users u on a.id = u.account WHERE username=:username AND status=1');
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
        $user= new User( $fetch['accountId']);
        PasswordUtils::verify($password, $fetch['password']);
        return $response->withJson(new SuccessResponse(array('Logged'=>$fetch['id'])));

    } catch (BadFunctionCallException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_BAD_REQUEST), Response::HTTP_BAD_REQUEST);
    } catch (UnauthorizedException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    }
});

$app->post('/session/login/admin', function (SlimRequest $request, SlimResponse $response, $args) {
    global $dbs, $fps;
    $json = $request->getBody();
    $data = json_decode($json, true);

    try {
        $username = $data['username'];
        $password = $data['password'];

        if (empty($username)) {
            throw new BadFunctionCallException("Username cannot be empty");
        } elseif (empty($password)) {
            throw new BadFunctionCallException("Password cannot be empty");
        }

        $stmt = $dbs['comunica']->prepare('SELECT u.id, a.id as accountId, a.username, a.password FROM user_accounts a INNER JOIN users u on a.id = u.account WHERE username=:username AND status=1 and type=2');
        $stmt->bindParam(':username', $username);
        $stmt->execute();
        $fetch = $stmt->fetch(PDO::FETCH_ASSOC);
        $user= new User( $fetch['accountId']);
        PasswordUtils::verify($password, $fetch['password']);
        return $response->withJson(new SuccessResponse(array('Logged'=>$fetch['id'])));

    } catch (BadFunctionCallException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_BAD_REQUEST), Response::HTTP_BAD_REQUEST);
    } catch (UnauthorizedException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    }
});