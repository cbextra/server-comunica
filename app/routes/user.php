<?php

use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;


$app->post('/user/password-recovery', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $email = $request->getParsedBody()['email'];

        return $response->withJson(new SuccessResponse(User::recoveryPassword($email)));
      //  return $response->withJson(new SuccessResponse($request->getParsedBody()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->post('/user', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $userDetails = $request->getParsedBody();

        $user = new User(0);

        return $response->withJson(new SuccessResponse($user->createUser($userDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/users', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $user = new User(0);

        return $response->withJson(new SuccessResponse($user->getAllUsers()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/user/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $userId = $args['userId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->getUserDetails()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});



$app->post('/user/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $userDetails = $request->getParsedBody();

        $userId = $args['userId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->updateUser($userDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});




$app->post('/user/password/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $userDetails = $request->getParsedBody();

        $userId = $args['userId'];

        $user = new User($userId);

        $oldPassword = $userDetails['oldPassword'];
        $newPassword = $userDetails['newPassword'];

        return $response->withJson(new SuccessResponse($user->updatePassword($oldPassword, $newPassword)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->delete('/user/{userId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $userId = $args['userId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->deleteUser()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/user/{userId}/visibility/{status}', function (SlimRequest $request, SlimResponse $response, $args) {
    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $userId = $args['userId'];
        $status= $args['status'];
        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->setUserVisibility($status)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/user/{userId}/topic/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $userId = $args['userId'];
        $topicId = $args['topicId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->addTopic($topicId)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/user/{userId}/topics', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $userId = $args['userId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->getTopics()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->get('/users/deleted-accounts', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $user = new User(0);

        return $response->withJson(new SuccessResponse($user->getDeteletdAccounts()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->delete('/user/{userId}/topic/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $userId = $args['userId'];
        $topicId = $args['topicId'];

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->deleteTopic($topicId)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/user/{userId}/deleteAccount', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        $sessionPayload = SessionManager::checkSession($request);
        if($sessionPayload){
            $userId=$sessionPayload->sub;
        }else {
            $userId=NULL;
        }

        $user = new User($userId);

        return $response->withJson(new SuccessResponse($user->deleteAccount()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});
