<?php


use Slim\Http\Request as SlimRequest;
use Slim\Http\Response as SlimResponse;


$app->post('/comunication', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);
        $comunicationnDetail = $request->getParsedBody();

        $comunication = new Comunication(0);

        return $response->withJson(new SuccessResponse($comunication->addComunication($comunicationnDetail)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->get('/comunication/{comunicationId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $comunicationId = $args['comunicationId'];

        $comunication = new Comunication($comunicationId);

        return $response->withJson(new SuccessResponse($comunication->getComunicationDetails()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->get('/comunications/{topicId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $comunication = new Comunication(0);
        $topicId = $args['topicId'];
        return $response->withJson(new SuccessResponse($comunication->getTopicComunications($topicId)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->get('/comunications', function (SlimRequest $request, SlimResponse $response, $args) {

    try {

        $sessionPayload = SessionManager::checkSession($request);
        if($sessionPayload){
            $userId=$sessionPayload->sub;
        }else {
            $userId=NULL;
        }
        $params=$request->getQueryParams();
        if(isset($params['term'])){
            $title=$params['term'];
        }else{
            $title=NULL;
        }

        $comunication = new Comunication(0);

        return $response->withJson(new SuccessResponse($comunication->getAllcomunications($title, $userId)));
        //return $response->withJson(new SuccessResponse($userId));


    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

$app->post('/comunication/{comunicationId}/visibility/{status}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $comunicationId = $args['comunicationId'];
        $status= $args['status'];
        $comunication = new Comunication($comunicationId);

        return $response->withJson(new SuccessResponse($comunication->setComunicationVisibility($status)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->post('/comunication/{comunicationId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        //$sessionPayload = SessionManager::checkSession($request);

        $comunicationDetails = $request->getParsedBody();

        $notificatonId = $args['comunicationId'];

        $comunication = new Comunication($notificatonId);

        return $response->withJson(new SuccessResponse($comunication->updatecomunication($comunicationDetails)));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


$app->delete('/comunication/{comunicationId}', function (SlimRequest $request, SlimResponse $response, $args) {

    try {
        $comunicationId = $args['comunicationId'];
        $comunication = new Comunication($comunicationId);
        return $response->withJson(new SuccessResponse($comunication->deletecomunication()));

    } catch (UnexpectedValueException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_UNATHORIZED), Response::HTTP_UNATHORIZED);
    } catch (NotExistsException $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_NOT_FOUND), Response::HTTP_NOT_FOUND);
    } catch (Exception $e) {
        return $response->withJson(new ErrorResponse($e, Response::HTTP_INTERNAL_SERVER_ERROR), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});


