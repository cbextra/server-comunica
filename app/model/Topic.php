<?php
/**
 * Created by PhpStorm.
 * User: R.Valente
 * Date: 01/08/2018
 * Time: 13:36
 */

class Topic
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function addTopic(array $topicDetails)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $query = $fps['comunica']->insertInto('topics')->values($topicDetails);
            $query->execute();

            $topicId = $fps['comunica']->getPdo()->lastInsertId();

            $fps['comunica']->getPdo()->commit();

            return $topicId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function getTopicDetails()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT name, description FROM topics  WHERE id=:topicId');
            $stmt->bindParam(':topicId', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            $topicDetails = $stmt->fetch(PDO::FETCH_ASSOC);

            return $topicDetails;

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getAllTopics($name = NULL)
    {
        global $dbs, $fps;

        try {

            if ($name == NULL) {
                $stmt = $dbs['comunica']->prepare('SELECT id, name AS text, description FROM topics');
                $stmt->execute();
                $topics = $stmt->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $stmt = $dbs['comunica']->prepare('SELECT id, name AS text, description FROM topics  WHERE name LIKE :name');
                $stmt->bindValue(':name', "%$name%");
                $stmt->execute();
                $topics = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }

            return $topics;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getAllUserTopics($userId)
    {
        global $dbs, $fps;

        try {
            $stmt = $dbs['comunica']->prepare('SELECT id, name, description FROM topics');
            $stmt->execute();
            $topics = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($topics as $topic) {
                $stmt2 = $dbs['comunica']->prepare('SELECT * FROM users_rel_topics WHERE user_id=:userId and topic_id=:topicId');
                $stmt2->bindParam(':userId', $userId, PDO::PARAM_INT);
                $stmt2->bindParam(':topicId', $topic['id'], PDO::PARAM_INT);
                $stmt2->execute();
                if ($stmt2->rowCount() > 0) {
                    $topic['registered']='1';
                } else {
                    $topic['registered']='0';
                }
                $result[] =$topic;

            }
            //return $topicsUsers;
            return $result;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function updateTopic(array $topicDetails)
    {
        global $dbs, $fps;

        try {

            $fps['comunica']->getPdo()->beginTransaction();

            $query = $fps['comunica']->update('topics')->set($topicDetails)->where('id', $this->id);
            $query->execute();

            $fps['comunica']->getPdo()->commit();

            return $this->id;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function deleteTopic()
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $stmt = $dbs['comunica']->prepare('DELETE FROM topics WHERE id=:topicId');
            $stmt->bindParam(':topicId', $this->id, PDO::PARAM_INT);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $this->id;;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }
}