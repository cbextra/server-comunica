<?php

class Event
{

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }


    public function uploadPicture()
    {
        global $dbs, $fps;

        try {
            if (!isset($_FILES['file'])) {
                throw new BadFunctionCallException("File required");
            }

            //Recupero il percorso temporaneo del file
            $tmpPath = $_FILES['file']['tmp_name'];

            //recupero il nome originale del file caricato
            $fileName = $_FILES['file']['name'];
            $fileName = microtime(true) . '_' . str_replace(" ", "_", $fileName);


            // per prima cosa verifico che il file sia stato effettivamente caricato
            if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
                throw new BadFunctionCallException('File not uploaded');
            }

            //Verifica del mime-type del file caricato
            $finfo = new finfo(FILEINFO_MIME_TYPE);

            $mime = $finfo->file($_FILES['file']['tmp_name']);

            $allowedMimes = [
                'jpeg' => 'image/jpeg', 'jpg' => 'image/jpeg',
                'png' => 'image/png'
            ];

            if (false === $ext = array_search($mime, $allowedMimes, true)) {
                throw new BadFunctionCallException("File not allowed");
            }

            if (!file_exists(IMAGES_PATH)) {
                mkdir(IMAGES_PATH, 0755, true);
            }

            $picture = THIS_SERVER . DS . IMAGES_DIR . DS . $fileName;
            $uploadPath = IMAGES_PATH . DS . $fileName;

            //Copio il file dalla sua posizione temporanea alla mia cartella upload
            if (!move_uploaded_file($tmpPath, $uploadPath)) {
                throw new BadFunctionCallException('File move failed');
            }

            return $picture;

        } catch (PDOException $e) {
            throw $e;
        } catch (BadFunctionCallException $e) {
            throw $e;
        }
    }

    public function addEvent(array $eventDetails)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $query = $fps['comunica']->insertInto('events')->values($eventDetails);
            $query->execute();

            $eventId = $fps['comunica']->getPdo()->lastInsertId();

            $fps['comunica']->getPdo()->commit();

            return $eventId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function getEventDetails()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT * FROM events  WHERE id=:eventId AND visibility=1');
            $stmt->bindParam(':eventId', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            $eventsDetails = $stmt->fetch(PDO::FETCH_ASSOC);

            return $eventsDetails;

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getAllEvents($name = NULL, $userId = NULL)
    {
        global $dbs, $fps;

        try {

            if ($userId != NULL) {
                $user = new User($userId);
                $userType = $user->getUserType($userId);
                if ($userType != "2") {
                    $stmt = $dbs['comunica']->prepare('SELECT * FROM events where visibility=1');
                    $stmt->execute();
                    $events = $stmt->fetchAll(PDO::FETCH_ASSOC);

                } else {

                    $stmt = $dbs['comunica']->prepare('SELECT * FROM events');
                    $stmt->execute();
                    $events = $stmt->fetchAll(PDO::FETCH_ASSOC);

                }

                return $events;
            }
        } catch
        (PDOException $e) {
            throw $e;
        }
    }


    public
    function updateEvent(array $eventDetails)
    {
        global $dbs, $fps;

        try {

            $fps['comunica']->getPdo()->beginTransaction();

            $query = $fps['comunica']->update('events')->set($eventDetails)->where('id', $this->id);
            $query->execute();

            $fps['comunica']->getPdo()->commit();

            return $this->id;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public
    function deleteEvent()
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $stmt = $dbs['comunica']->prepare('UPDATE events SET visibility=0  WHERE id=:eventId');
            $stmt->bindParam(':eventId', $this->id, PDO::PARAM_INT);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $this->id;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public
    function setEventVisibility($status)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $eventId = $this->id;

            $stmt = $dbs['comunica']->prepare('UPDATE events SET visibility =:status  WHERE id =:eventId');
            $stmt->bindParam(':eventId', $eventId, PDO::PARAM_INT);
            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $eventId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

}
