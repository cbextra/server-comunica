<?php

/**
 * Created by PhpStorm.
 * User: R.Valente
 * Date: 24/07/2018
 * Time: 09:42
 */
class Comunication
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function addComunication(array $comunicationDetail)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $query = $fps['comunica']->insertInto('comunications')->values($comunicationDetail);
            $query->execute();

            $comunicationId = $fps['comunica']->getPdo()->lastInsertId();

            $fps['comunica']->getPdo()->commit();

            return $comunicationId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function getComunicationDetails()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT comunications.id, title, comunications.description, visibility, send_date, topic_id, topics.NAME AS topic_name, topics.description AS topic_description  FROM comunications, topics  WHERE comunications.id=:comunicationId and comunications.topic_id=topics.id');
            $stmt->bindParam(':comunicationId', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            $comunicationDetail = $stmt->fetch(PDO::FETCH_ASSOC);

            return $comunicationDetail;

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getTopicComunications($topicId)
    {
        global $dbs, $fps;
        try {
            $stmt = $dbs['comunica']->prepare('SELECT comunications.id, title, comunications.description FROM comunications INNER JOIN topics on comunications.topic_id=topics.id
              WHERE  comunications.topic_id=:topicId and visibility=1');
            $stmt->bindParam(':topicId', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $comunications = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $comunications;
        } catch (PDOException $e) {
            throw $e;
        }
    }





public
function getAllcomunications($name = NULL, $userId = NULL)
{
    global $dbs, $fps;

    try {
        if ($userId != NULL) {
            $user = new User($userId);
            $userType = $user->getUserType($userId);
            if ($userType != "2") {
                $comunications = array();
                $stmtu = $dbs['comunica']->prepare('SELECT topic_id from users_rel_topics where user_id=:userId');
                $stmtu->bindParam(':userId', $userId, PDO::PARAM_INT);
                $stmtu->execute();
                $topics = $stmtu->fetchAll(PDO::FETCH_ASSOC);

                foreach ($topics as $topic) {
                    $stmt = $dbs['comunica']->prepare('SELECT id, title AS text, description, send_date FROM comunications where topic_id=:topicId and visibility=1 ');
                    $stmt->bindParam(':topicId', $topic['topic_id']);
                    $stmt->execute();
                    $comunication = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $comunications = array_merge($comunications, $comunication);
                }

                function sortFunction($a, $b) {
                    return strtotime($b['send_date']) - strtotime($a['send_date']);
                }

                usort($comunications, 'sortFunction');

                return $comunications;
            } else {
                $stmt = $dbs['comunica']->prepare('SELECT comunications.id, title AS text, comunications.description, send_date, visibility, topics.name FROM comunications, topics where comunications.topic_id=topics.id');
                $stmt->execute();
                $comunications = $stmt->fetchAll(PDO::FETCH_ASSOC);
                return $comunications;
            }
        }
        //  return $userId;
    } catch (PDOException $e) {
        throw $e;
    }
}


public
function updatecomunication(array $comunicationDetail)
{
    global $dbs, $fps;

    try {

        $fps['comunica']->getPdo()->beginTransaction();

        $query = $fps['comunica']->update('comunications')->set($comunicationDetail)->where('id', $this->id);
        $query->execute();

        $fps['comunica']->getPdo()->commit();

        return $this->id;

    } catch (PDOException $e) {
        $fps['comunica']->getPdo()->rollBack();
        throw $e;
    }
}


public
function deletecomunication()
{
    global $dbs, $fps;

    try {
        $fps['comunica']->getPdo()->beginTransaction();

        $comunicationId = $this->id;

        $stmt = $dbs['comunica']->prepare('UPDATE comunications SET visibility = 0  WHERE id =:comunicationId');
        $stmt->bindParam(':comunicationId', $comunicationId, PDO::PARAM_INT);
        $stmt->execute();

        $fps['comunica']->getPdo()->commit();

        return $comunicationId;

    } catch (PDOException $e) {
        $fps['comunica']->getPdo()->rollBack();
        throw $e;
    } catch (BadFunctionCallException $e) {
        $fps['comunica']->getPdo()->rollBack();
        throw $e;
    }
}

    public function setComunicationVisibility($status)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $comunicationId = $this->id;

            $stmt = $dbs['comunica']->prepare('UPDATE comunications SET visibility =:status  WHERE id =:comunicationId');
            $stmt->bindParam(':comunicationId', $comunicationId, PDO::PARAM_INT);
            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $comunicationId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }
}