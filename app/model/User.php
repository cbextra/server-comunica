<?php

class User
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    public function getUserType($userId)
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT * FROM users u INNER JOIN user_accounts ua ON u.account=ua.id WHERE u.id=:userId');
            $stmt->bindParam(':userId', $userId);
            $stmt->execute();
            $userType = $stmt->fetch(PDO::FETCH_ASSOC)[type];
            return $userType;

        } catch (PDOException $e) {
            throw $e;
        } catch (BadFunctionCallException $e) {
            throw $e;
        }
    }

    public static function recoveryPassword($email)
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT u.name, u.surname, u.email, ua.username, ua.id AS accountId FROM users u INNER JOIN user_accounts ua ON u.account=ua.id WHERE u.email=:email AND u.deleted=0');
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $userDetails = $stmt->fetch(PDO::FETCH_ASSOC);

            $username = $userDetails['username'];
            unset($userDetails['username']);
            $accountId = $userDetails['accountId'];
            unset($userDetails['accountId']);

            $newPassword = CodeGenerator::generateUserCode($username, 8);
            //$newPassword='peppe';

            $userDetails['newPassword'] = $newPassword;

            $stmt = $dbs['comunica']->prepare('UPDATE user_accounts SET password=:newPassword WHERE id=:accountId');
            $stmt->bindParam(':accountId', $accountId, PDO::PARAM_INT);
            $stmt->bindParam(':newPassword', PasswordUtils::hash($newPassword));
            $stmt->execute();

            $mail = new Mail($userDetails);

            $mail->send();

            return $userDetails['email'];
           // return $email;

        } catch (PDOException $e) {
            throw $e;
        } catch (BadFunctionCallException $e) {
            throw $e;
        }
    }

    public function createUser(array $userDetails)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $username = $userDetails['username'];
            $password = $userDetails['password'];

            if (empty($username)) {
                throw new BadFunctionCallException("Username cannot be empty");
            } elseif (empty($password)) {
                throw new BadFunctionCallException("Password cannot be empty");
            }

            $stmt = $dbs['comunica']->prepare('SELECT id FROM user_accounts WHERE username=:username');
            $stmt->bindParam(':username', $username);
            $stmt->execute();
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if (empty($fetch)) {
                $stmt = $dbs['comunica']->prepare('INSERT INTO user_accounts (username, password) VALUES (:username, :password)');
                $stmt->bindParam(':username', $username);
                $stmt->bindParam(':password', PasswordUtils::hash($password));
                $stmt->execute();
                $account = $dbs['comunica']->lastInsertId();

                $userDetails['account'] = $account;
                unset($userDetails['username']);
                unset($userDetails['password']);

                $query = $fps['comunica']->insertInto('users')->values($userDetails);

                $query->execute();


            } else {
                throw new BadFunctionCallException("Username $username already used");
            }

            $userId = $fps['comunica']->getPdo()->lastInsertId();

            $stmtT = $dbs['comunica']->prepare('INSERT INTO users_rel_topics  VALUES (:userId, 1)');
            $stmtT->bindParam(':userId', $userId);
            $stmtT->execute();
            //$mail=new Mail($userId, $userDetails['name'], $userDetails['surname'], $userDetails['username'], $userDetails['password']);

            //$mail->send($userDetails['email']);

            $fps['comunica']->getPdo()->commit();

            return $userId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public function getAllUsers()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT users.id, name, surname, address, phone_number, email, fiscal_code, organization, picture_url, push_id, status FROM users, user_accounts  WHERE users.account=user_accounts.id and deleted=0');
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $users;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getDeteletdAccounts()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT users.id, name, surname, email, date_requested  FROM users, deleted_users  WHERE users.id=deleted_users.user_id');
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $users;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getUserDetails()
    {
        global $dbs, $fps;

        try {

            $stmt = $dbs['comunica']->prepare('SELECT id, name, surname, address, phone_number, email, fiscal_code, organization, picture_url FROM users  WHERE id=:userId AND deleted=0');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->execute();
            $userDetails = $stmt->fetch(PDO::FETCH_ASSOC);

            return $userDetails;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function updateUser(array $userDetails)
    {
        global $dbs, $fps;

        try {
            $email = $userDetails['email'];

            $stmt = $dbs['comunica']->prepare('SELECT id FROM users WHERE id!=:userId AND email=:email');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if (empty($fetch)) {
                $fps['comunica']->getPdo()->beginTransaction();
                $query = $fps['comunica']->update('users')->set($userDetails)->where('id', $this->userId);
                $query->execute();
            } else {
                throw new BadFunctionCallException("Username $email already used");
            }


            $fps['comunica']->getPdo()->commit();

            return $this->userId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public function updatePassword($oldPassword, $newPassword)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            if (empty($oldPassword)) {
                throw new BadFunctionCallException("Old password cannot be empty");
            } elseif (empty($newPassword)) {
                throw new BadFunctionCallException("New password cannot be empty");
            }


            $stmt = $dbs['comunica']->prepare('SELECT a.id, a.password FROM user_accounts a INNER JOIN users u ON u.account=a.id WHERE u.id=:userId');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->execute();
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            $accountId = $fetch['id'];
            $password = $fetch['password'];


            if (PasswordUtils::match($oldPassword, $password)) {
                $stmt = $dbs['comunica']->prepare('UPDATE user_accounts SET password=:newPassword  WHERE id=:accountId');
                $stmt->bindParam(':newPassword', PasswordUtils::hash($newPassword));
                $stmt->bindParam(':accountId', $accountId, PDO::PARAM_INT);
                $stmt->execute();
            } else {
                throw new BadFunctionCallException("Password not match");
            }

            $fps['comunica']->getPdo()->commit();

            return $accountId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public function deleteUser()
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $userId = $this->userId;

            $stmt = $dbs['comunica']->prepare('SELECT a.id FROM user_accounts a INNER JOIN users u ON u.account=a.id WHERE u.id=:userId');
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
            $stmt->execute();
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            $accountId = $fetch['id'];

            $stmt = $dbs['comunica']->prepare('UPDATE user_accounts SET status=0  WHERE id=:accountId');
            $stmt->bindParam(':accountId', $accountId, PDO::PARAM_INT);
            $stmt->execute();

            $stmt = $dbs['comunica']->prepare('UPDATE users SET deleted=1  WHERE id=:userId');
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $userId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function addTopic($topicId)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $stmt = $dbs['comunica']->prepare('SELECT * FROM users_rel_topics WHERE user_id=:userId AND topic_id=:topicId');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->bindParam(':topicId', $topicId, PDO::PARAM_INT);
            $stmt->execute();
            $fetch = $stmt->fetch(PDO::FETCH_ASSOC);

            if (empty($fetch)) {
                $stmt = $dbs['comunica']->prepare('INSERT INTO users_rel_topics (user_id, topic_id) VALUES (:userId, :topicId)');
                $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
                $stmt->bindParam(':topicId', $topicId, PDO::PARAM_INT);
                $stmt->execute();
            }

            $fps['comunica']->getPdo()->commit();

            return array('userId' => $this->userId, 'topicId' => $topicId);

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public function getTopics()
    {
        global $dbs, $fps;

        try {
            $stmt = $dbs['comunica']->prepare('SELECT t.*, ut.user_id FROM topics t INNER JOIN users_rel_topics ut ON t.id=ut.topic_id WHERE user_id=:userId');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->execute();
            $comunications = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $comunications;

        } catch (PDOException $e) {
            throw $e;
        } catch (BadFunctionCallException $e) {
            throw $e;
        }
    }

    public function deleteTopic($topicId)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $stmt = $dbs['comunica']->prepare('DELETE FROM users_rel_topics WHERE user_id=:userId AND topic_id=:topicId');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->bindParam(':topicId', $topicId, PDO::PARAM_INT);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return array('userId' => $this->userId, 'promoId' => $topicId);

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }


    public function getAccountId()
    {
        global $dbs, $fps;

        try {
            $stmt = $dbs['comunica']->prepare('SELECT  a.id as accountId FROM user_accounts a INNER JOIN users u on a.id = u.account WHERE u.id=:userId AND status=1 ');
            $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_ASSOC)[accountId];
            return $user;

        } catch (PDOException $e) {
            throw $e;
        } catch (BadFunctionCallException $e) {
            throw $e;
        }
    }

    public function setUserVisibility($status)
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $userId = $this->id;
            $accountId = $this->getAccountId();
            $stmt = $dbs['comunica']->prepare('UPDATE user_accounts SET status=:status  WHERE id=:accountId');
            $stmt->bindParam(':accountId', $accountId, PDO::PARAM_INT);
            $stmt->bindParam(':status', $status, PDO::PARAM_STR);
            $stmt->execute();

            $fps['comunica']->getPdo()->commit();

            return $userId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }

    public function deleteAccount()
    {
        global $dbs, $fps;

        try {
            $fps['comunica']->getPdo()->beginTransaction();

            $userId = $this->id;
            $accountId = $this->getAccountId();
            $stmt = $dbs['comunica']->prepare('UPDATE user_accounts SET status=0  WHERE id=:accountId');
            $stmt->bindParam(':accountId', $accountId, PDO::PARAM_INT);
            $stmt->execute();
            $fps['comunica']->getPdo()->commit();

            $stmt2 = $dbs['comunica']->prepare('INSERT INTO deleted_users (user_id) VALUES (:userId)');
            $stmt2->bindParam(':userId', $this->userId, PDO::PARAM_INT);
            $stmt2->execute();

            return $userId;

        } catch (PDOException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        } catch (BadFunctionCallException $e) {
            $fps['comunica']->getPdo()->rollBack();
            throw $e;
        }
    }
}
