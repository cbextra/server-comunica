<?php

class UserBlockedException extends Exception {
	protected $message = "User is blocked";
}