<?php

class FileUploadException extends Exception {
	protected $message = "File not uploaded";
}