<?php

class FileNotFoundException extends Exception {
	protected $message = "File not found";
}
