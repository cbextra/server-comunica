<?php

class ForbiddenException extends Exception {
	protected $message = "Not allowed to see this content";
}
