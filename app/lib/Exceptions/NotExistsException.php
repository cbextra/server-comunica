<?php

class NotExistsException extends Exception {
    protected $message = "Item does not exist";
}
