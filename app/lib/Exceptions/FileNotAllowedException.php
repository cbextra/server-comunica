<?php

class FileNotAllowedException extends FileUploadException {
	protected $message = "File is not allowed";
}
