<?php

/**
 * Created by PhpStorm.
 * User: F.Stoppiello
 * Date: 14/06/2017
 * Time: 17:20
 */


//require __DIR__.'/../vendor/autoload.php';

define('MAIL', __DIR__.'/mail.html');

define('MAILTXT',  __DIR__.'/mail.txt');


class Mail {

    private $destName, $destSurname,  $destEmail, $newPassword;

    public function __construct(array $userDetails){

        $this->destName = $userDetails['name'];
        $this->destSurname = $userDetails['surname'];
        $this->destEmail = $userDetails['email'];
        $this->newPassword = $userDetails['newPassword'];
    }

    public function send() {
        global $dbh;

        date_default_timezone_set('Europe/Rome');


        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';

        $mail->isSMTP();                                      // Set mailer to use SMTP

        $mail->SMTPDebug = 1;
        $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->Port = 587;                                    // TCP port to connect to
        $mail->SMTPSecure = 'tsl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPAuth = true;                               // Enable SMTP authentication

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Username = 'comuncampochiaro@gmail.com';    // SMTP username
        $mail->Password = 'campochiaro2018';                 // SMTP password

        $mail->setFrom('comunicampochiaro@gmail.com', 'Campochiaro Comunica');

        $mail->addAddress($this->destEmail, $this->destName . " " . $this->destSurname);

        $m = new Mustache_Engine;

        $mailDetails = array('userName' => ucfirst($this->destName), 'userSurname' => ucfirst($this->destSurname), 'newPassword' => $this->newPassword);

       // $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Password Recovery';

        $mail->Body = $m->render(file_get_contents(MAILTXT), $mailDetails);

        //$mail->AltBody = $m->render(file_get_contents(MAILTXT), $userDetails);

        if (!$mail->send()) {
            throw new BadFunctionCallException($mail->ErrorInfo);
        }

        //$mail->clearAddresses();

        /*$stmt = $dbs['conference']->prepare("UPDATE user SET reg_code = :regCode WHERE user_id = :userId");
        $stmt->bindParam(':regCode', $regCode);
        $stmt->bindParam(':userId', $this->userId, PDO::PARAM_INT);
        $stmt->execute();*/

        return true;
    }

    public function testAuth(){

        $mail = new PHPMailer(true);
        $mail->SMTPAuth = true;
        $mail->Username = 'comunica@comune.campochiaro.gov.it';
        $mail->Password = '20Comunica18$';
        $mail->Host = 'smtps.aruba.it';
        $mail->Port = 465;

// This function returns TRUE if authentication
// was successful, or throws an exception otherwise
        try {
            $validCredentials = $mail->SmtpConnect();
        }
        catch(Exception $error) {
           return $error;
        }
        return $validCredentials;
    }
}
