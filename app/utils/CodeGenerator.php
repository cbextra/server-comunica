<?php


class CodeGenerator {

	public static function generateUserCode($str, $len) {
		$str .= microtime();
		return substr(sprintf("%u", crc32($str)), 0, $len);
	}

	public static function generateOperatorCode($str, $len) {
		$str .= microtime();
		return substr(sprintf("%u", crc32($str)), 0, $len);
	}

}
