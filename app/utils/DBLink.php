<?php


/*"conference": {
    "driver": "mysql",
    "host": "root_videoconference-db_1",
    "dbName": "unipegaso_conference",
    "username": "root",
    "password": "Qazxcdert8"
},
"pegaso": {
    "driver": "mysql",
    "host": "lms.unipegaso.it",
    "dbName": "iUniversity_dokeos_main",
    "username": "videoconference",
    "password": "videoconference2016;"
},
"mercatorum": {
    "driver": "mysql",
    "host": "213.171.162.146",
    "dbName": "iUniversity_dokeos_main",
    "username": "videoconference",
    "password": "videoconference2016;"
}*/


class DBLink extends PDO {

    /**
     * DBLink constructor.
     * @param $dbName "The connection name"
     * @throws Exception
     */


    public function __construct($dbName) {

        // Getting DB configurations
        if (!file_exists(APP . "/db.config.json")) {
            throw new Exception("Database configuration file does not exist");
        }

        $configurations = json_decode(file_get_contents(APP . "/db.config.json"));

        if (!isset($configurations->{$dbName})) {
            throw  new Exception("Database configuration does not exist");
        }

        $connectionDetails = $configurations->{$dbName};

        if($connectionDetails->driver=="sqlsrv"){
            $dsn = "sqlsrv:server=$connectionDetails->server,$connectionDetails->port;Database=$connectionDetails->database";
        }else{
            $dsn = "$connectionDetails->driver:host=$connectionDetails->host;dbname=$connectionDetails->dbName;charset=utf8";
        }

        return parent::__construct($dsn, $connectionDetails->username, $connectionDetails->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    /**
     * @return DBLink[]
     * @throws Exception
     */
    public static function connectAll() {

        $pdos = [];

        if (!file_exists(APP . "/db.config.json")) {
            throw new Exception("Database configuration file does not exist");
        }

        $configurations = json_decode(file_get_contents(APP . "/db.config.json"), true);

        foreach ($configurations as $db => $connectionDetails) {
            $pdos[$db] = new DBLink($db);
        }

        return $pdos;

    }
}
