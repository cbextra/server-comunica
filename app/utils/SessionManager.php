<?php

use Slim\Http\Request as SlimRequest;

class SessionManager {

    const JWT_KEY = 'GvhYAA8ZrlVz1cAobWpcV2DldW4WSaz5';
    const SESSION_EXPIRE_MINUTES = 5000000;

    public static function checkSession(SlimRequest $request) {

        if (!$request->hasHeader('Authorization')) {

            throw new UnexpectedValueException('No token provided');
        }

        $token = preg_replace('/^Bearer\\s/', '', $request->getHeader('Authorization')[0]);
        return JWT::decode($token, self::JWT_KEY, array('HS256'));

    }


    public static function issueToken($userId) {
        return JWT::encode(array(
            "sub" => $userId,
            "iss" => "http://pegasointernational.com/",
            "exp" => (time() + (self::SESSION_EXPIRE_MINUTES * 60)) * 50,
            "nbf" => time()
        ), self::JWT_KEY, 'HS256');
    }


    public static function getPlatformId($issuer) {

        if (!file_exists(APP . "/platforms.config.json")) {
            throw new Exception("Platforms configuration file does not exist");
        }

        $platforms = json_decode(file_get_contents(APP . "/platforms.config.json"), true);

        if (!key_exists($issuer, $platforms)) {
            throw new Exception("Platform configuration for " . $issuer . " does not exist");
        }

        return $platforms[$issuer]['id'];

    }

    public static function getPlatformName($issuer) {

        if (!file_exists(APP . "/platforms.config.json")) {
            throw new Exception("Platforms configuration file does not exist");
        }

        $platforms = json_decode(file_get_contents(APP . "/platforms.config.json"), true);

        if (!key_exists($issuer, $platforms)) {
            throw new Exception("Platform configuration for " . $issuer . " does not exist");
        }

        return $platforms[$issuer]['name'];

    }
}
